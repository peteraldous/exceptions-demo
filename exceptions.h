#include <iostream>

using namespace std;

class NumberFormatException : public exception {
    public:
        const char *n;
        NumberFormatException(const char *s) : n(s) {
        }
        const char *what();
};

class OneLineException : public exception {
    public:
        const char *s;
        OneLineException(const char *str) : s(str) {
        }
        const char *what();
};

class TooManyLinesException : public exception {
    public:
        const char *s;
        TooManyLinesException(const char *str) : s(str) {
        }
        const char *what();
};

class student
{
    public:
        student(const string &n, const unsigned int id)
            : name(n), ID(id)
        {
        }
        student(const student &s)
            : name(s.name), ID(s.ID)
        {
        }
        ~student()
        {
        }
        student &operator=(const student &s);

        const string &getName() const;
        unsigned int getID() const;

        static student *parse(const string &record);

        friend ostream &operator<<(ostream &,
                const student &);
    private:
        string name;
        unsigned int ID;

        static int parseID(const char *str);
};
