#include <assert.h>

#include "exceptions.h"

using namespace std;

int main() {
    student *s;
    try
    {
        s = student::parse("Charlie Brown\n1234");
    }
    catch(...)
    {
        assert(false && "threw an exception");
    }
    assert(s->getName() == "Charlie Brown");
    assert(s->getID() == 1234U);
    delete s;
    s = NULL;
    try
    {
        s = student::parse("No newline");
        assert(false && "did not fail");
    }
    catch(OneLineException &ole)
    {
    }
    cout << "Woot. You passed." << endl;
    return 0;
}
