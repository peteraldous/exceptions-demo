#include "exceptions.h"

using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        return 1;
    }
    string record(argv[1]);
    student *s;
    try
    {
        s = student::parse(record);
    }
    catch(TooManyLinesException &tmle)
    {
        cout << "Too many lines in \"" << record << "\"" << endl;
        return 1;
    }
    catch(OneLineException &ole)
    {
        cout << "No newline in \"" << record << "\"" << endl;
        return 2;
    }
    catch(NumberFormatException &nfe)
    {
        cout << "Number format exception in \"" << record << "\": " <<
            nfe.what() << endl;
        return 3;
    }
    catch(...)
    {
        cout << "There was a problem but I have no idea what it was." << endl;
        return 4;
    }
    cout << *s << endl;
    delete s;
    return 0;
}

