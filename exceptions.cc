#include "exceptions.h"

using namespace std;

const char *NumberFormatException::what()
{
    return n;
}

const char *OneLineException::what()
{
    return "OneLineException";
}

const char *TooManyLinesException::what()
{
    return "TooManyLinesException";
}

student &student::operator=(const student &s)
{
    name = s.name;
    ID = s.ID;
    return *this;
}

const string &student::getName() const
{
    return name;
}

unsigned int student::getID() const
{
    return ID;
}

student *student::parse(const string &record)
{
    const size_t separator_index = record.find_first_of('\n');
    if (separator_index == string::npos)
    {
        throw OneLineException("No newline");
    }
    if (record.find_last_of('\n') != separator_index)
    {
        throw TooManyLinesException("too many");
    }
    string name = record.substr(0U, separator_index);

    size_t id_length = record.length()-separator_index;
    if (id_length <=0)
    {
        throw NumberFormatException("No number to parse");
    }
    char buffer[id_length];
    record.copy(buffer, id_length, separator_index+1U);
    int ID = parseID(buffer);
    return new student(name, (unsigned int) ID);
}

int student::parseID(const char *str) {
    if (str[0] < '0' || str[0] > '9')
    {
        throw NumberFormatException("Not a number");
    }
    int ID = atoi(str);
    if (ID < 0)
    {
        throw NumberFormatException("Negative ID");
    }
    return ID;
}

ostream &operator<<(ostream &os, const student &s) {
    os << "Student: " << s.name << "\n" << "ID: " << s.ID;
    return os;
}
