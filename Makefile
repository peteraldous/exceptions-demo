

make_student: exceptions.cc exceptions.h make_student.cc
	g++ -o make_student -Wall -Werror -g exceptions.cc make_student.cc

test_student: exceptions.cc exceptions.h test_student.cc
	g++ -o test_student -Wall -Werror -g exceptions.cc test_student.cc

.PHONY: test
test: test_student
	./test_student

.PHONY: clean
clean:
	rm make_student test_student
