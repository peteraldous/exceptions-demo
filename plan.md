# Tip: Polymorphism

* Virtual functions require overhead. Use them, but only when they serve a purpose.
* Remember that polymorphism only works on pointers.

# Exceptions

* Walk through the main function and talk about the format for a student:
    - Two lines
    - First line has a student's full name
    - Second line has a student's ID number
    - `main()` reads in a student and prints it with `cout <<`
* Use the code to motivate:
    - Errors aren't specific
    - Code is kind of a mess
* Refactor the code into exceptions
    - Use OneLineException when there's no newline
    - Use TooManyLinesException when there are multiple newlines
    - Use NumberFormatException when the second line isn't a number
    - Replace check for `NULL` in `main()` with `try/catch`
* Discuss when exceptions are most useful
    - When using third-party code that may fail
    - When errors may occur (unpredictably)
    - When an error would cause a chain of returns and checked values
    - When it makes code easier to understand
    - Do not use in constructors/destructors (in C++)
* Briefly show the table of common exceptions
* Mention that the std::exception is a thing but that anything can be thrown

# Testing

* Errors don't always crop up in every execution
* A few methods for testing:
    - Structured walkthrough (go through the code - possibly with a bug)
    - Unit/integration/system/acceptance
    - Black-box and White-box
* Build a few tests in `test_student.cc` (using assertions)
* Lots of people may test, including devs, testers, and users

# Debugging

* Brief gdb demo

# Loop invariants

* Something that doesn't change as the loop executes repeatedly
